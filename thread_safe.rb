require 'hamster'

class Inventory
  def initialize(stock_levels)
    @stock = Hamster.hash stock_levels
  end

  def decrease(item)
    @stock[item] -= 1
  end

  def [](item)
    @stock[item]
  end
end

inventory = Inventory.new tshirt: 200, pants: 340, hats: 4000

threads = Array.new
40.times do
  threads << Thread.new do
    100.times do
      inventory.decrease(:hats)
    end
  end
end

puts "HATS: #{inventory[:hats]}"
