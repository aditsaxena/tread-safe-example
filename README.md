# Threadsafe test #

This is a simple implementation test for the Hamster gem, a collection of 6 classes to help dealing with thread safety.

Here I only tried the hash class, to demonstrate how easy is to avoid basic common problems in multi-thread environment.

Another common approach in concurrency is to use Mutexes.  
They are a good starting point although in some environment you encounter a common problem with pessimistic locks, when a resource takes too long to process slowing down the whole flow.

### References ###

* [Hamster gem](https://github.com/hamstergem/hamster)
* [Ruby Conf 2013 - Advanced Concurrent Programming in Ruby](https://www.youtube.com/watch?v=VVmENBTc1jM)
* [Thread Safety First!](https://www.youtube.com/watch?v=d7UeQ8XoYeo)

### Alternatives ###
* [Celluloid (Actor-based framework)](https://github.com/celluloid/celluloid)
